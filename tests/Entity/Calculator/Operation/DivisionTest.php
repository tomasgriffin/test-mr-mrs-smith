<?php


use App\Entity\Calculator\Operation\Division;
use PHPUnit\Framework\TestCase;

class DivisionTest extends TestCase
{
    /**
     * @var Division
     */
    protected $subject;

    public function setUp()
    {
        $this->subject = new Division();
    }

    public function testGetTotal()
    {
        $this->assertEquals(
            1.5,
            $this->subject->getTotal(3, 2)
        );
    }
}
