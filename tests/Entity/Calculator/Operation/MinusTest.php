<?php


use App\Entity\Calculator\Operation\Minus;
use PHPUnit\Framework\TestCase;

class MinusTest extends TestCase
{
    /**
     * @var Minus
     */
    protected $subject;

    public function setUp()
    {
        $this->subject = new Minus();
    }

    public function testGetTotal()
    {
        $this->assertEquals(
            1,
            $this->subject->getTotal(3, 2)
        );
    }
}
