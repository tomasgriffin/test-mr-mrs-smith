<?php


use App\Entity\Calculator\Operation\Multiplication;
use PHPUnit\Framework\TestCase;

class MultiplicationTest extends TestCase
{
    /**
     * @var Multiplication
     */
    protected $subject;

    public function setUp()
    {
        $this->subject = new Multiplication();
    }

    public function testGetTotal()
    {
        $this->assertEquals(
            6,
            $this->subject->getTotal(3, 2)
        );
    }
}
