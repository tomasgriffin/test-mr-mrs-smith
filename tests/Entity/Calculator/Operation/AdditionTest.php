<?php


use App\Entity\Calculator\Operation\Addition;
use PHPUnit\Framework\TestCase;

class AdditionTest extends TestCase
{
    /**
     * @var Addition
     */
    protected $subject;

    public function setUp()
    {
        $this->subject = new Addition();
    }

    public function testGetTotal()
    {
        $this->assertEquals(
            4,
            $this->subject->getTotal(2, 2)
        );
    }
}
