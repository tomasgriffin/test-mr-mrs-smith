# Mr & Mrs Smith Exercise

## Installation

```
composer install
```

## Run

```
php -S 127.0.0.1:8000 -t public
```

## View

The application calculator is viewable at `http://127.0.0.1:8000/calculator`
