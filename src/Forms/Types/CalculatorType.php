<?php

declare(strict_types=1);

namespace App\Forms\Types;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CalculatorType extends AbstractType
{
    protected $value1;

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder->add('value1', TextType::class)
            ->add('operation', ChoiceType::class, [
                'choices' => $builder->getData()->getOperations(),
                'choice_label' => 'label',
            ])
            ->add('value2', TextType::class)
            ->add('submit', SubmitType::class);
    }

    public function getValue1()
    {
        return $this->value1;
    }
}