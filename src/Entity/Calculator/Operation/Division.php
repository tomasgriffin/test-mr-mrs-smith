<?php


namespace App\Entity\Calculator\Operation;


class Division implements OperationInterface
{
    public function getLabel(): string
    {
        return 'divide';
    }

    public function getTotal(float $value1, float $value2): float
    {
        return $value1 / $value2;
    }
}