<?php


namespace App\Entity\Calculator\Operation;


class Multiplication implements OperationInterface
{
    public function getLabel(): string
    {
        return 'multiply';
    }

    public function getTotal(float $value1, float $value2): float
    {
        return $value1 * $value2;
    }
}