<?php

namespace App\Entity\Calculator\Operation;

interface OperationInterface
{
    /**
     * @return string
     */
    public function getLabel(): string;

    /**
     * @param float $value1
     * @param float $value2
     *
     * @return float
     */
    public function getTotal(float $value1, float $value2): float;
}