<?php


namespace App\Entity\Calculator\Operation;


class Minus implements OperationInterface
{
    public function getLabel(): string
    {
        return 'minus';
    }

    public function getTotal(float $value1, float $value2): float
    {
        return $value1 - $value2;
    }
}