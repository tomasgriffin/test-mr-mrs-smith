<?php

namespace App\Entity;

use App\Entity\Calculator\Operation\Addition;
use App\Entity\Calculator\Operation\Division;
use App\Entity\Calculator\Operation\Minus;
use App\Entity\Calculator\Operation\Multiplication;
use App\Entity\Calculator\Operation\OperationInterface;

class Calculator
{
    /**
     * @var float
     */
    protected $value1 = 0;

    /**
     * @var float
     */
    protected $value2 = 0;

    /**
     * @var OperationInterface
     */
    protected $operation = null;

    /**
     * @var array
     */
    protected $operations;

    public function __construct()
    {
        $this->operations = [
            new Addition(),
            new Minus(),
            new Multiplication(),
            new Division(),
        ];
    }

    /**
     * @return float
     */
    public function getValue1(): float
    {
        return $this->value1;
    }

    /**
     * @return float
     */
    public function setValue1(float $value1): Calculator
    {
        $this->value1 = $value1;

        return $this;
    }

    /**
     * @return float
     */
    public function getValue2(): float
    {
        return $this->value2;
    }

    /**
     * @return float
     */
    public function setValue2(float $value2): Calculator
    {
        $this->value2 = $value2;

        return $this;
    }

    /**
     * @return OperationInterface
     */
    public function getOperation(): ?OperationInterface
    {
        return $this->operation;
    }

    /**
     * @return OperationInterface
     */
    public function setOperation(OperationInterface $operation): Calculator
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * @return array
     */
    public function getOperations(): array
    {
        return $this->operations;
    }
}