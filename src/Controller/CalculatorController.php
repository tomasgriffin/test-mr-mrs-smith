<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Calculator;
use App\Forms\Types\CalculatorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CalculatorController extends AbstractController
{
    public function execute(Request $request)
    {
        $calculatorEntity = new Calculator();

        $form = $this->createForm(CalculatorType::class, $calculatorEntity);
        $form->handleRequest($request);

        $result = null;

        if ($form->isSubmitted() && $form->isValid())
        {
            $result = $calculatorEntity->getOperation()->getTotal(
                $calculatorEntity->getValue1(),
                $calculatorEntity->getValue2()
            );
        }

        return $this->render(
            'calculator.html.twig',
            [
                'form' => $form->createView(),
                'result' => $result,
            ]
        );
    }
}